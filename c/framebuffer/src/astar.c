/* astar.c 2013.04.18 Thursday -- gl
   Paint a block on screen
*/
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int
main( int argc, char **argv ) {

  //  printf ( "starting\n" );

  /* assume: a screen of 1024 by 768,
             a pixel of red, green, blue, alpha components
   */

#define Pwrong   0xaa112200l
#define P        0x002211aal

  int   c = 512, climit, w = 32, r = 384, rlimit, h = 32, l;
  long  b[1024 * 768];
  /* (addition) Initialise the output */
  for ( l = 0 ; l < ( 1024 * 768 ) ; l++ ) {
    b[l] = 0l;
  }

  /* A'1. Set the block limits */
  climit = c + w;
  rlimit = r + h;

  //  printf ( "climit: %d, rlimit: %d\n", climit, rlimit );
  /* A'2. Start a row */
  l = ( r * 1024 ) + c;

  //  printf ( "l: (%d)\n", l);
  
  /* A'3. Set the pixel */
 a3:  b[l] = P;
  //  printf ( "P: (%lx)\n", P );

  /* A'4. Row done? */
  if ( c < climit ) {
    //    printf ( "c: %d\n", c );
    l++;
    c++;
    goto a3;
  }

  /* A'5. Block done? */
  if ( r < rlimit ) {
    //    printf ( "r: %d\n", r );
    r++;
    c -= w;
    l = l + 1024 - w;
    goto a3;
  }

  write ( 1, b, 1024*768*4 );
  return 0;

}

int
usage( void ) {
#define USAGE   "./astar > OUTPUT_FILE\n" \
    "\n"								\
    "DESCRIPTION\n"							\
    "\twrite a colour square to STDOUT.\n"				\
    "\tThe OUTPUT_FILE is intended to be suitable to be placed within the frame buffer.\n" \
    "\n"								\
    "\tcp OUTPUT_1FILE /dev/fb0\n"

  printf( USAGE );
}
