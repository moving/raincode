/* 2013-03-28 1:26 atoi

 */

#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char **argv)
{
  int c;
  printf("Hello, here-> goes\n");

#define THREE_OF_THOSE(x)  x, x, x
#define THREE_OF_THOSE_FORMAT   "%c => %d => %x\n"

  /*  printf( "%c => %d => %x\n", ':', ':', ':' ); */

  /*  printf( "%c =< %d => %x\n", THREE_OF_THOSE(':') ); */

  for ( c = 0x30; c < 0x40; c++ )
    printf( THREE_OF_THOSE_FORMAT, THREE_OF_THOSE( c ));

  return 0;
}

