/* 2013-03-23 gl

   feature_test_macros experience

   make syconf-test
   cc   syconf-test.c -o sysconf-test

   #define  __USE_GNU

   TODO: more to read on these subjects

*/

#include <unistd.h>
#include <limits.h>
#include <stdio.h>

int main( int argc, char **arv ) {

  long _posix_version = sysconf( _POSIX_VERSION );
  long _sc_version    = sysconf( _SC_VERSION );

  printf( "Hello, world\n\n"
	  "_POSIX_VERSION = (%d)\n"
	  "_SC_VERSION    = (%d)\n",
	  _posix_version,
	  _sc_version );

  return 0;

}
